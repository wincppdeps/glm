cmake_minimum_required(VERSION 2.8)

message(STATUS "========================================================")
message(STATUS "START GLM")
message(STATUS "========================================================")

add_subdirectory(src)

INSTALL(FILES FindGLM.cmake DESTINATION cmake-modules COMPONENT Devel)

message(STATUS "========================================================")
message(STATUS "END GLM")
message(STATUS "========================================================")
